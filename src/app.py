"""Web blog app."""
from flask import Flask


app = Flask(__name__)


@app.route('/')
def hello_method():
    """Hya, folks."""
    return "Hello, world!"


if __name__ == "__main__":
    app.run(debug=True)
