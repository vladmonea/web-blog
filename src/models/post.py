"""Post handler"""
import uuid
import datetime

from ..common.database import Database
from .common.database import Database


class Post(object):
    """This class defines properties and methods for Posts."""
    def __init__(self, blog_id, title, content, author, date=datetime.datetime.utcnow(), _id=None):
        self.blog_id = blog_id
        self.title = title
        self.content = content
        self.author = author
        self.created_date = date
        self._id = uuid.uuid4().hex if _id is None else _id

    def save_to_mongo(self):
        """Save Post data to MongoDB."""
        Database.insert(collection='posts',
                        data=self.json())

    def json(self):
        """Create a JSON representation of the Post."""
        return {
            '_id': self._id,
            'blog_id': self.blog_id,
            'author': self.author,
            'content': self.content,
            'title': self.title,
            'created_date': self.created_date
        }

    @classmethod
    def from_mongo(cls, id):
        post_data = Database.find_one(collection='posts',
                                      query={"_id": id})
        return cls(blog_id=post_data['blog_id'],
                   title=post_data['title'],
                   content=post_data['content'],
                   author=post_data['author'],
                   date=post_data['created_date'],
                   id=post_data['_id'])

    @staticmethod
    def from_blog(id):
        return [post for post in Database.find(collection='posts',
                                               query={"blog_id": id})]

    @staticmethod
    def from_author(author):
        return [post for post in Database.find(collection='posts',
                                               query={"author": author})]
