"""User handler."""
from flask import session

from src.common.database import Database


class User:
    def __init__(self, email, password, _id):
        self.email = email
        self.password = password
        self._id = uuid.uudi4().hex if _id in None else _id

    @classmethod
    def get_by_email(cls, email):
        data = Database.find_one(collection='users',
                                 query={"email": email})
        return cls(**data) if data is not None

    @classmethod
    def get_by_id(cls, _id):
        data = Database.find_one(collection='users',
                                 query={"_id": _id})
        return cls(**data) if data is not None

    @staticmethod
    def login_valid(email, password):
        """Check whether a user's email/password combination is valid.
        Example:
        User.login_valid("vlad@domain.com", "my_pass")

        :return type: bool
        """
        user = User.get_by_email(email)
        if user is not None:
            # Check password
            return user.password == password
        return False

    @classmethod
    def register(cls, email, password):
        user = cls.get_by_email(email)
        if user is None:
            # User doesn't exist, so we can create it
            new_user = cls(email, password)
            new_user.save_to_mongo()
            session['email'] = email
            return True
        else:
            # User exists :(
            return False

    @staticmethod
    def login(user_email):
        # login_valid has already been called
        # 
        session['email'] = user_email

    @staticmethod
    def logout():
        session['email'] = None

    def get_blogs(self):
        pass

    def json(self):
        return {
            "email": self.email,
            "password": self.password,
            "_id": self._id
        }

    def save_to_mongo(self):
        Database.insert(collection='users',
                        data=self.json())
